package SplitWithRecursion;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		System.out.println("Enter a sentence");
		String sentence = in.nextLine();
		System.out.println("Enter the split word");
		String split = in.nextLine();
		List<String> result = new ArrayList<String>();
		splitWithRecursion(result, split, sentence);
		displayList(result);

	}
	public static List<String> splitWithRecursion(List<String> result , String splitstr,String str){
		int lengthsplit = splitstr.length();
		int length = str.length();
		int firstAppearenceOfSplit = str.indexOf(splitstr);
		StringBuffer strbuff = new StringBuffer();
		StringBuffer strbuffOther = new StringBuffer();
		if(firstAppearenceOfSplit == -1) {
			result.add(str);
			return result;
		}
		else {
			for(int i = 0;i<firstAppearenceOfSplit;i++) {
				strbuff.append(str.charAt(i));
			}
			result.add(strbuff.toString());
			for(int i = firstAppearenceOfSplit + lengthsplit;i<length;i++) {
				strbuffOther.append(str.charAt(i));
			}
			splitWithRecursion(result,splitstr,strbuffOther.toString());
		}
		
		return null;
	}
	
	public static void displayList(List<String> list) {
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + "  ");
		}
	}

}
